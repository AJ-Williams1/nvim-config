" Initialization: {{{
highlight clear
if exists("syntax_on")
    syntax reset
endif
set background=dark
set g:colors_name="farmhouse"
" }}}
" Palette: {{{
let s:gb = {}

let s:gb.gray0   = ['#1D1D29',]
let s:gb.gray1   = ['#272b34',]
let s:gb.gray2   = ['#495259',]
let s:gb.gray4   = ['#7b8383',]
let s:gb.gray5   = ['#9c9b95',]
let s:gb.gray6   = ['#c0beb9',]
let s:gb.gray7   = ['#dedbd7',]
let s:gb.gray8   = ['#ede9e7',]
let s:gb.gray9   = ['#f6f2f3',]
let s:gb.red1    = ['#9e000d',]
let s:gb.red2    = ['#c60013',]
let s:gb.red3    = ['#ef001b',]
let s:gb.yellow1 = ['#b7690a',]
let s:gb.yellow2 = ['#eea806',]
let s:gb.yellow3 = ['#fcce16',]
let s:gb.green1  = ['#4a8b0d',]
let s:gb.green2  = ['#8cc610',]
let s:gb.green3  = ['#b0e60e',]
let s:gb.cyan1   = ['#2c9f6b',]
let s:gb.cyan2   = ['#1ee079',]
let s:gb.cyan3   = ['#2aed75',]
let s:gb.blue1   = ['#124cd3',]
let s:gb.blue2   = ['#088afb',]
let s:gb.blue3   = ['#23cbfe',]
let s:gb.purple1 = ['#941458',]
let s:gb.purple2 = ['#ec3691',]
let s:gb.purple3 = ['#fa5fa8',]
" }}}
